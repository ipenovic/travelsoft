﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TravelSoft
{
    public partial class TravelSoft : Form
    {
        public TravelSoft()
        {
            InitializeComponent();
        }

        private void TravelSoft_Load(object sender, EventArgs e)
        {
            RefreshLists();
        }

        private void RefreshLists()
        {
            lblOdabraniObjekt.Text = "";
            cmbOdaberiGrad.Items.Clear();
            cmbOdaberiGrad.Text = "";
            lsbObjekti.Items.Clear();
            lsbGosti.Items.Clear();
            lsbSobe.Items.Clear();
            lsbRezervacije.Items.Clear();

            using (var context = new TravelSoftEntities())
            {
                var objekti = context.Objekts
                    .Select(o => o.grad)
                    .Distinct();

                var gosti = context.Gosts;

                foreach (var o in objekti)
                    cmbOdaberiGrad.Items.Add(o);

                foreach (var g in gosti)
                    lsbGosti.Items.Add(g.ime + " " + g.prezime);
            }

            using (var context = new TravelSoftEntities())
            {
                var rezervacija = context.Rezervacijas.ToList<Rezervacija>();

                foreach (var r in rezervacija)
                {
                    var objekt = context.Objekts
                        .Where(o => o.objektId == r.objektId)
                        .First<Objekt>();

                    var soba = context.Sobas
                        .Where(s => s.sobaId == r.sobaId)
                        .First<Soba>();

                    var gost = context.Gosts
                        .Where(g => g.gostId == r.gostId)
                        .First<Gost>();

                    lsbRezervacije.Items.Add(r.rezervacijaId + " - " + r.datumRezervacije.ToString("dd/MM/yyyy") + " - " + gost.prezime + " - " + objekt.imeObjetka);
                }
            }

            if (lsbObjekti.SelectedItem == null)
                btnDodajNovuSobu.Enabled = false;
            else
                btnDodajNovuSobu.Enabled = true;

            btnBrisiRez.Enabled = false;
            btnDetaljiRez.Enabled = false;
        }

        private void cmbOdaberiGrad_SelectedIndexChanged(object sender, EventArgs e)
        {
            using (var context = new TravelSoftEntities())
            {
                var query = context.Objekts
                    .Where(q => q.grad == cmbOdaberiGrad.SelectedItem.ToString())
                    .ToList<Objekt>();

                lsbObjekti.Items.Clear();
                foreach (var q in query)
                    lsbObjekti.Items.Add(q.imeObjetka + " - broj soba: " + q.brojSoba);
            }
            lsbSobe.Items.Clear();
            btnRezervacija.Enabled = false;
        }

        private void btnDodajNovi_Click(object sender, EventArgs e)
        {
            NoviObjekt frmNovi = new NoviObjekt();
            frmNovi.ShowDialog();
            frmNovi.Dispose();
            RefreshLists();
        }

        public string objekt = "";
        private void btnUrediObjekt_Click(object sender, EventArgs e)
        {
            try
            {
                string[] odabrano = lsbObjekti.SelectedItem.ToString().Split('-');
                objekt = odabrano[0];
                UrediObjekt frmUredi = new UrediObjekt(objekt);
                frmUredi.ShowDialog();
                frmUredi.Dispose();
                RefreshLists();
            }
            catch { }
        }

        private void btnDodajNovogGosta_Click(object sender, EventArgs e)
        {
            NoviGost frmNovi = new NoviGost();
            frmNovi.ShowDialog();
            frmNovi.Dispose();
            RefreshLists();
        }

        public string ime = "";
        public string prezime = "";
        private void btnUrediGosta_Click(object sender, EventArgs e)
        {
            try
            {   
                UrediGosta frmUredi = new UrediGosta(ime, prezime);
                frmUredi.ShowDialog();
                frmUredi.Dispose();
                RefreshLists();
            }
            catch { }
        }

        public int brObjekta = 0;

        private void lsbObjekti_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                string vrijednost = lsbObjekti.SelectedItem.ToString();
                string[] objekt = vrijednost.Split('-');
                string imeObjekta = objekt[0];

                using (var context = new TravelSoftEntities())
                {
                    var query = context.Objekts
                        .Where(o => o.imeObjetka == imeObjekta)
                        .FirstOrDefault<Objekt>();
                    lblOdabraniObjekt.Text = "Odabrani objekt: " + query.imeObjetka;
                    brObjekta = query.objektId;

                    var sobe = context.Sobas
                        .Where(s => s.objektId == brObjekta)
                        .ToList<Soba>();

                    lsbSobe.Items.Clear();

                    if (sobe.Count == 0)
                        lsbSobe.Items.Add("Objekt trenutno nema unesenih soba");
                    else
                        foreach (var s in sobe)
                            lsbSobe.Items.Add(s.sobaId + ". soba - broj kreveta: " + s.brojKreveta + " - cijena: " + s.cijena);

                    if (sobe.Count < query.brojSoba)
                        btnDodajNovuSobu.Enabled = true;
                    else
                        btnDodajNovuSobu.Enabled = false;
                }
            }
            catch { }
        }

        private void btnDodajNovuSobu_Click(object sender, EventArgs e)
        {
            NovaSoba frmNova = new NovaSoba(brObjekta);
            frmNova.ShowDialog();
            frmNova.Dispose();
            RefreshLists();
        }

        public int sobaId = 0;
        private void lsbSobe_SelectedIndexChanged(object sender, EventArgs e)
        {
            botunRezervacija();

            try
            {
                string[] oznaceno = lsbSobe.SelectedItem.ToString().Split('.');
                sobaId = int.Parse(oznaceno[0]);
            }
            catch{ }
        }

        private void lsbGosti_SelectedIndexChanged(object sender, EventArgs e)
        {
            botunRezervacija();

            try
            {
                string[] odabrano = lsbGosti.SelectedItem.ToString().Split(' ');
                ime = odabrano[0];
                prezime = odabrano[1];
            }
            catch { }
        }

        private void btnRezervacija_Click(object sender, EventArgs e)
        {
            Random r = new Random();
            int rezervacijeId = r.Next(1000);

            using (var context = new TravelSoftEntities())
            {
                var odabraniObjekt = context.Objekts
                    .Where(o => o.objektId == brObjekta)
                    .First<Objekt>();

                var odabranaSoba = context.Sobas
                    .Where(s => s.sobaId == sobaId)
                    .First<Soba>();

                var odabraniGost = context.Gosts
                    .Where(g => g.ime == ime && g.prezime == prezime)
                    .First<Gost>();

                var rezervacija = new Rezervacija()
                {
                    rezervacijaId = r.Next(1000),
                    gostId = odabraniGost.gostId,
                    objektId = odabraniObjekt.objektId,
                    sobaId = odabranaSoba.sobaId,
                    datumRezervacije = DateTime.Now,
                    pocetakRezervacije = dateTimePicker1.Value,
                    krajRezervacije = dateTimePicker2.Value
                };
                context.Rezervacijas.Add(rezervacija);
                context.SaveChanges();

                RefreshLists();
            }
        }

        private void btnBrisiRez_Click(object sender, EventArgs e)
        {
            using (var context = new TravelSoftEntities())
            {
                var query = context.Rezervacijas
                    .Where(r => r.rezervacijaId == Idrezervacija)
                    .FirstOrDefault<Rezervacija>();
                context.Rezervacijas.Remove(query);
                context.SaveChanges();
                RefreshLists();
            }
        }

        public int Idrezervacija = 0;
        private void lsbRezervacije_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                btnBrisiRez.Enabled = true;
                btnDetaljiRez.Enabled = true;

                string[] oznacen = lsbRezervacije.SelectedItem.ToString().Split('-');
                Idrezervacija = int.Parse(oznacen[0]);
            }
            catch { }
        }

        private void btnDetaljiRez_Click(object sender, EventArgs e)
        {
            DetaljiRez frmDetalji = new DetaljiRez(Idrezervacija);
            frmDetalji.ShowDialog();
        }

        private void dateTimePicker2_ValueChanged(object sender, EventArgs e)
        {
            botunRezervacija();
        }

        private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
        {
            botunRezervacija();
        }

        private void botunRezervacija()
        {
            if (dateTimePicker2.Value > dateTimePicker1.Value && lsbObjekti.SelectedItem != null && lsbSobe.SelectedItem != null && lsbGosti.SelectedItem != null)
                btnRezervacija.Enabled = true;
            else
                btnRezervacija.Enabled = false;
        }
    }
}
