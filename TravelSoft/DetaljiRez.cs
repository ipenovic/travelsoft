﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TravelSoft
{
    public partial class DetaljiRez : Form
    {
        public int _idrez { get; set; }
        public DetaljiRez(int Idrez)
        {
            InitializeComponent();
            _idrez = Idrez;
        }

        private void DetaljiRez_Load(object sender, EventArgs e)
        {
            using (var context = new TravelSoftEntities())
            {
                var rezervacija = context.Rezervacijas
                    .Where(r => r.rezervacijaId == _idrez)
                    .First<Rezervacija>();

                var objekt = context.Objekts
                        .Where(o => o.objektId == rezervacija.objektId)
                        .First<Objekt>();

                var soba = context.Sobas
                    .Where(s => s.sobaId == rezervacija.sobaId)
                    .First<Soba>();

                var gost = context.Gosts
                    .Where(g => g.gostId == rezervacija.gostId)
                    .First<Gost>();

                int dani = (rezervacija.krajRezervacije.Value - rezervacija.pocetakRezervacije.Value).Days;

                lblInfo.Text = "Podaci o gostu:\n====================\nIme:"
                    + gost.ime + "\nPrezime:" + gost.prezime + "\nDob: " + gost.godine
                    + "\n\n\nPodaci o objektu:\n====================\nObjekt: "
                    + objekt.imeObjetka + "\nGrad: " + objekt.grad
                    + "\nPočetak rezervacije: " + rezervacija.pocetakRezervacije.Value.ToString("dd/MM/yyyy")
                    + "\nKraj rezervacije: " + rezervacija.krajRezervacije.Value.ToString("dd/MM/yyyy")
                    + "\n\n\nDatum i vrijeme rezervacije:\n====================\n"
                    + rezervacija.datumRezervacije.ToString("dd/MM/yyyy") + "\n"
                    + rezervacija.datumRezervacije.ToString("HH:mm:ss")
                    + "\n\n\nCijena:\n====================\nBroj noćenja: " + dani
                    + "\nCijena po večeri: " + soba.cijena + "kn/noć"
                    + "\nUkupna cijena: " + soba.cijena * dani + " kn";
            }            
        }
    }
}
