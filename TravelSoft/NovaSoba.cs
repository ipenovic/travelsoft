﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TravelSoft
{
    public partial class NovaSoba : Form
    {
        public int _objektId { get; set; }
        public NovaSoba(int objektId)
        {
            InitializeComponent();
            _objektId = objektId;
        }

        private void btnNoviObjekt_Click(object sender, EventArgs e)
        {
            try
            {
                var context = new TravelSoftEntities();
                var soba = new Soba()
                {
                    sobaId = int.Parse(textBox1.Text),
                    objektId = int.Parse(textBox2.Text),
                    brojKreveta = int.Parse(textBox3.Text),
                    cijena = int.Parse(textBox4.Text)
                };
                context.Sobas.Add(soba);
                context.SaveChanges();
                this.Close();
            }
            catch
            {
                lblPoruka.Text = "Molimo provjerite uensene podatke!";
            }
        }

        private void NovaSoba_Load(object sender, EventArgs e)
        {
            Random r = new Random();
            textBox1.Text = r.Next(1000).ToString();
            textBox2.Text = _objektId.ToString();
        }
    }
}
