﻿namespace TravelSoft
{
    partial class TravelSoft
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnUrediObjekt = new System.Windows.Forms.Button();
            this.btnDodajNovi = new System.Windows.Forms.Button();
            this.lsbObjekti = new System.Windows.Forms.ListBox();
            this.cmbOdaberiGrad = new System.Windows.Forms.ComboBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label2 = new System.Windows.Forms.Label();
            this.btnUrediGosta = new System.Windows.Forms.Button();
            this.btnDodajNovogGosta = new System.Windows.Forms.Button();
            this.lsbGosti = new System.Windows.Forms.ListBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.lblOdabraniObjekt = new System.Windows.Forms.Label();
            this.btnDodajNovuSobu = new System.Windows.Forms.Button();
            this.lsbSobe = new System.Windows.Forms.ListBox();
            this.btnRezervacija = new System.Windows.Forms.Button();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.btnBrisiRez = new System.Windows.Forms.Button();
            this.btnDetaljiRez = new System.Windows.Forms.Button();
            this.lsbRezervacije = new System.Windows.Forms.ListBox();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.dateTimePicker2 = new System.Windows.Forms.DateTimePicker();
            this.label3 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.btnUrediObjekt);
            this.groupBox1.Controls.Add(this.btnDodajNovi);
            this.groupBox1.Controls.Add(this.lsbObjekti);
            this.groupBox1.Controls.Add(this.cmbOdaberiGrad);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(223, 213);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Objekti";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(71, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Odaberi grad:";
            // 
            // btnUrediObjekt
            // 
            this.btnUrediObjekt.Location = new System.Drawing.Point(6, 176);
            this.btnUrediObjekt.Name = "btnUrediObjekt";
            this.btnUrediObjekt.Size = new System.Drawing.Size(200, 23);
            this.btnUrediObjekt.TabIndex = 3;
            this.btnUrediObjekt.Text = "Uredi objekt";
            this.btnUrediObjekt.UseVisualStyleBackColor = true;
            this.btnUrediObjekt.Click += new System.EventHandler(this.btnUrediObjekt_Click);
            // 
            // btnDodajNovi
            // 
            this.btnDodajNovi.Location = new System.Drawing.Point(6, 147);
            this.btnDodajNovi.Name = "btnDodajNovi";
            this.btnDodajNovi.Size = new System.Drawing.Size(200, 23);
            this.btnDodajNovi.TabIndex = 2;
            this.btnDodajNovi.Text = "Dodaj objekt";
            this.btnDodajNovi.UseVisualStyleBackColor = true;
            this.btnDodajNovi.Click += new System.EventHandler(this.btnDodajNovi_Click);
            // 
            // lsbObjekti
            // 
            this.lsbObjekti.FormattingEnabled = true;
            this.lsbObjekti.Location = new System.Drawing.Point(6, 46);
            this.lsbObjekti.Name = "lsbObjekti";
            this.lsbObjekti.Size = new System.Drawing.Size(200, 95);
            this.lsbObjekti.TabIndex = 1;
            this.lsbObjekti.SelectedIndexChanged += new System.EventHandler(this.lsbObjekti_SelectedIndexChanged);
            // 
            // cmbOdaberiGrad
            // 
            this.cmbOdaberiGrad.FormattingEnabled = true;
            this.cmbOdaberiGrad.Location = new System.Drawing.Point(85, 19);
            this.cmbOdaberiGrad.Name = "cmbOdaberiGrad";
            this.cmbOdaberiGrad.Size = new System.Drawing.Size(121, 21);
            this.cmbOdaberiGrad.TabIndex = 0;
            this.cmbOdaberiGrad.SelectedIndexChanged += new System.EventHandler(this.cmbOdaberiGrad_SelectedIndexChanged);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.btnUrediGosta);
            this.groupBox2.Controls.Add(this.btnDodajNovogGosta);
            this.groupBox2.Controls.Add(this.lsbGosti);
            this.groupBox2.Location = new System.Drawing.Point(241, 12);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(223, 262);
            this.groupBox2.TabIndex = 5;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Gosti";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 22);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(76, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Odaberi gosta:";
            // 
            // btnUrediGosta
            // 
            this.btnUrediGosta.Location = new System.Drawing.Point(6, 232);
            this.btnUrediGosta.Name = "btnUrediGosta";
            this.btnUrediGosta.Size = new System.Drawing.Size(200, 23);
            this.btnUrediGosta.TabIndex = 3;
            this.btnUrediGosta.Text = "Uredi gosta";
            this.btnUrediGosta.UseVisualStyleBackColor = true;
            this.btnUrediGosta.Click += new System.EventHandler(this.btnUrediGosta_Click);
            // 
            // btnDodajNovogGosta
            // 
            this.btnDodajNovogGosta.Location = new System.Drawing.Point(6, 203);
            this.btnDodajNovogGosta.Name = "btnDodajNovogGosta";
            this.btnDodajNovogGosta.Size = new System.Drawing.Size(200, 23);
            this.btnDodajNovogGosta.TabIndex = 2;
            this.btnDodajNovogGosta.Text = "Dodaj gosta";
            this.btnDodajNovogGosta.UseVisualStyleBackColor = true;
            this.btnDodajNovogGosta.Click += new System.EventHandler(this.btnDodajNovogGosta_Click);
            // 
            // lsbGosti
            // 
            this.lsbGosti.FormattingEnabled = true;
            this.lsbGosti.Location = new System.Drawing.Point(6, 46);
            this.lsbGosti.Name = "lsbGosti";
            this.lsbGosti.Size = new System.Drawing.Size(200, 147);
            this.lsbGosti.TabIndex = 1;
            this.lsbGosti.SelectedIndexChanged += new System.EventHandler(this.lsbGosti_SelectedIndexChanged);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.lblOdabraniObjekt);
            this.groupBox3.Controls.Add(this.btnDodajNovuSobu);
            this.groupBox3.Controls.Add(this.lsbSobe);
            this.groupBox3.Location = new System.Drawing.Point(12, 231);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(223, 177);
            this.groupBox3.TabIndex = 6;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Sobe";
            // 
            // lblOdabraniObjekt
            // 
            this.lblOdabraniObjekt.AutoSize = true;
            this.lblOdabraniObjekt.Location = new System.Drawing.Point(6, 27);
            this.lblOdabraniObjekt.Name = "lblOdabraniObjekt";
            this.lblOdabraniObjekt.Size = new System.Drawing.Size(0, 13);
            this.lblOdabraniObjekt.TabIndex = 4;
            // 
            // btnDodajNovuSobu
            // 
            this.btnDodajNovuSobu.Location = new System.Drawing.Point(6, 147);
            this.btnDodajNovuSobu.Name = "btnDodajNovuSobu";
            this.btnDodajNovuSobu.Size = new System.Drawing.Size(200, 23);
            this.btnDodajNovuSobu.TabIndex = 2;
            this.btnDodajNovuSobu.Text = "Dodaj sobu";
            this.btnDodajNovuSobu.UseVisualStyleBackColor = true;
            this.btnDodajNovuSobu.Click += new System.EventHandler(this.btnDodajNovuSobu_Click);
            // 
            // lsbSobe
            // 
            this.lsbSobe.FormattingEnabled = true;
            this.lsbSobe.Location = new System.Drawing.Point(6, 46);
            this.lsbSobe.Name = "lsbSobe";
            this.lsbSobe.Size = new System.Drawing.Size(200, 95);
            this.lsbSobe.TabIndex = 1;
            this.lsbSobe.SelectedIndexChanged += new System.EventHandler(this.lsbSobe_SelectedIndexChanged);
            // 
            // btnRezervacija
            // 
            this.btnRezervacija.Enabled = false;
            this.btnRezervacija.Location = new System.Drawing.Point(250, 351);
            this.btnRezervacija.Name = "btnRezervacija";
            this.btnRezervacija.Size = new System.Drawing.Size(197, 50);
            this.btnRezervacija.TabIndex = 7;
            this.btnRezervacija.Text = "Napravi rezervaciju";
            this.btnRezervacija.UseVisualStyleBackColor = true;
            this.btnRezervacija.Click += new System.EventHandler(this.btnRezervacija_Click);
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.btnBrisiRez);
            this.groupBox4.Controls.Add(this.btnDetaljiRez);
            this.groupBox4.Controls.Add(this.lsbRezervacije);
            this.groupBox4.Location = new System.Drawing.Point(470, 12);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(331, 396);
            this.groupBox4.TabIndex = 8;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Rezervacije";
            // 
            // btnBrisiRez
            // 
            this.btnBrisiRez.Location = new System.Drawing.Point(6, 363);
            this.btnBrisiRez.Name = "btnBrisiRez";
            this.btnBrisiRez.Size = new System.Drawing.Size(319, 23);
            this.btnBrisiRez.TabIndex = 5;
            this.btnBrisiRez.Text = "Izbriši";
            this.btnBrisiRez.UseVisualStyleBackColor = true;
            this.btnBrisiRez.Click += new System.EventHandler(this.btnBrisiRez_Click);
            // 
            // btnDetaljiRez
            // 
            this.btnDetaljiRez.Location = new System.Drawing.Point(6, 334);
            this.btnDetaljiRez.Name = "btnDetaljiRez";
            this.btnDetaljiRez.Size = new System.Drawing.Size(319, 23);
            this.btnDetaljiRez.TabIndex = 4;
            this.btnDetaljiRez.Text = "Detalji";
            this.btnDetaljiRez.UseVisualStyleBackColor = true;
            this.btnDetaljiRez.Click += new System.EventHandler(this.btnDetaljiRez_Click);
            // 
            // lsbRezervacije
            // 
            this.lsbRezervacije.FormattingEnabled = true;
            this.lsbRezervacije.Location = new System.Drawing.Point(6, 19);
            this.lsbRezervacije.Name = "lsbRezervacije";
            this.lsbRezervacije.Size = new System.Drawing.Size(319, 303);
            this.lsbRezervacije.TabIndex = 2;
            this.lsbRezervacije.SelectedIndexChanged += new System.EventHandler(this.lsbRezervacije_SelectedIndexChanged);
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Location = new System.Drawing.Point(250, 299);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(197, 20);
            this.dateTimePicker1.TabIndex = 9;
            this.dateTimePicker1.ValueChanged += new System.EventHandler(this.dateTimePicker1_ValueChanged);
            // 
            // dateTimePicker2
            // 
            this.dateTimePicker2.Location = new System.Drawing.Point(250, 325);
            this.dateTimePicker2.Name = "dateTimePicker2";
            this.dateTimePicker2.Size = new System.Drawing.Size(197, 20);
            this.dateTimePicker2.TabIndex = 10;
            this.dateTimePicker2.ValueChanged += new System.EventHandler(this.dateTimePicker2_ValueChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(247, 277);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(139, 13);
            this.label3.TabIndex = 11;
            this.label3.Text = "Odaberi datume rezervacije:";
            // 
            // TravelSoft
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(813, 413);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.dateTimePicker2);
            this.Controls.Add(this.dateTimePicker1);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.btnRezervacija);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Name = "TravelSoft";
            this.Text = "TravelSoft";
            this.Load += new System.EventHandler(this.TravelSoft_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnUrediObjekt;
        private System.Windows.Forms.Button btnDodajNovi;
        private System.Windows.Forms.ListBox lsbObjekti;
        private System.Windows.Forms.ComboBox cmbOdaberiGrad;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnUrediGosta;
        private System.Windows.Forms.Button btnDodajNovogGosta;
        private System.Windows.Forms.ListBox lsbGosti;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label lblOdabraniObjekt;
        private System.Windows.Forms.Button btnDodajNovuSobu;
        private System.Windows.Forms.ListBox lsbSobe;
        private System.Windows.Forms.Button btnRezervacija;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Button btnBrisiRez;
        private System.Windows.Forms.Button btnDetaljiRez;
        private System.Windows.Forms.ListBox lsbRezervacije;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.DateTimePicker dateTimePicker2;
        private System.Windows.Forms.Label label3;
    }
}

