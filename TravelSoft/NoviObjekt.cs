﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TravelSoft
{
    public partial class NoviObjekt : Form
    {
        public NoviObjekt()
        {
            InitializeComponent();
        }

        private void btnNoviObjekt_Click(object sender, EventArgs e)
        {
            try
            {
                var context = new TravelSoftEntities();
                var objekt = new Objekt()
                {
                    objektId = int.Parse(textBox1.Text),
                    imeObjetka = textBox2.Text,
                    grad = textBox3.Text,
                    brojSoba = int.Parse(textBox4.Text)
                };
                context.Objekts.Add(objekt);
                context.SaveChanges();
                this.Close();
            }
            catch
            {
                lblPoruka.Text = "Molimo provjerite uensene podatke!";
            }
        }

        private void NoviObjekt_Load(object sender, EventArgs e)
        {
            Random r = new Random();
            textBox1.Text = r.Next(1000).ToString();
        }
    }
}
