﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TravelSoft
{
    public partial class UrediGosta : Form
    {
        public string _ime { get; set; }
        public string _prezime { get; set; }
        public UrediGosta(string ime, string prezime)
        {
            InitializeComponent();
            _ime = ime;
            _prezime = prezime;
        }

        private void UrediGosta_Load(object sender, EventArgs e)
        {
            using (var context = new TravelSoftEntities())
            {
                var query = context.Gosts
                                   .Where(g => g.ime == _ime && g.prezime == _prezime)
                                   .FirstOrDefault<Gost>();

                textBox1.Text = query.gostId.ToString();
                textBox2.Text = query.ime;
                textBox3.Text = query.prezime;
                textBox4.Text = query.godine.ToString();
                textBox5.Text = query.grad;
                textBox6.Text = query.drzava;
            }
        }

        private void btnSpremi_Click(object sender, EventArgs e)
        {
            try
            {
                using (var context = new TravelSoftEntities())
                {
                    var query = context.Gosts
                                        .Where(g => g.ime == _ime && g.prezime == _prezime)
                                        .FirstOrDefault<Gost>();
                    query.ime = textBox2.Text;
                    query.prezime = textBox3.Text;
                    query.godine = int.Parse(textBox4.Text);
                    query.grad = textBox5.Text;
                    query.drzava = textBox6.Text;
                    context.SaveChanges();
                    this.Close();
                }
            }
            catch
            {
                lblPoruka.Text = "Molimo provjerite unesene podatke!";
            }
        }

        private void btnBrisiGosta_Click(object sender, EventArgs e)
        {
            using (var context = new TravelSoftEntities())
            {
                var query = context.Gosts
                                    .Where(g => g.ime == _ime && g.prezime == _prezime)
                                    .FirstOrDefault<Gost>();
                context.Gosts.Remove(query);
                context.SaveChanges();
                this.Close();
            }
        }
    }
}
