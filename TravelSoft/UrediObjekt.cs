﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TravelSoft
{
    public partial class UrediObjekt : Form
    {
        public string imeObjekta { get; set; }
        public UrediObjekt(string objekt)
        {
            InitializeComponent();
            imeObjekta = objekt;
        }

        private void UrediObjekt_Load(object sender, EventArgs e)
        {
            using (var context = new TravelSoftEntities())
            {
                var query = context.Objekts
                                   .Where(o => o.imeObjetka == imeObjekta)
                                   .FirstOrDefault<Objekt>();

                textBox1.Text = query.objektId.ToString();
                textBox2.Text = query.imeObjetka;
                textBox3.Text = query.grad;
                textBox4.Text = query.brojSoba.ToString();
            }
        }

        private void btnSpremi_Click(object sender, EventArgs e)
        {
            try
            {
                using (var context = new TravelSoftEntities())
                {
                    var query = context.Objekts
                                        .Where(o => o.imeObjetka == imeObjekta)
                                        .FirstOrDefault<Objekt>();
                    query.imeObjetka = textBox2.Text;
                    query.grad = textBox3.Text;
                    query.brojSoba = int.Parse(textBox4.Text);
                    context.SaveChanges();
                    this.Close();
                }
            }
            catch
            {
                lblPoruka.Text = "Molimo provjerite unesene podatke!";
            }
        }

        private void btnBrisiObjekt_Click(object sender, EventArgs e)
        {
            using (var context = new TravelSoftEntities())
            {
                var query = context.Objekts
                                    .Where(o => o.imeObjetka == imeObjekta)
                                    .FirstOrDefault<Objekt>();
                context.Objekts.Remove(query);
                context.SaveChanges();
                this.Close();
            }
        }
    }
}
