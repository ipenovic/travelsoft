﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TravelSoft
{
    public partial class NoviGost : Form
    {
        public NoviGost()
        {
            InitializeComponent();
        }

        private void btnNoviObjekt_Click(object sender, EventArgs e)
        {
            try
            {
                var context = new TravelSoftEntities();
                var gost = new Gost()
                {
                    gostId = int.Parse(textBox1.Text),
                    ime = textBox2.Text,
                    prezime = textBox3.Text,
                    godine = int.Parse(textBox4.Text),
                    grad = textBox5.Text,
                    drzava = textBox6.Text
                };
                context.Gosts.Add(gost);
                context.SaveChanges();
                this.Close();
            }
            catch
            {
                lblPoruka.Text = "Molimo provjerite unesene podatke!";
            }
        }

        private void NoviGost_Load(object sender, EventArgs e)
        {
            Random r = new Random();
            textBox1.Text = r.Next(1000).ToString();
        }
    }
}
